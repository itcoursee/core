package core

// Price 价格
// 单位：分
// 如果是-1表示不允许0
type Price struct {
	// 播放价格
	Play int `json:"play" validate:"required,min=0,max=99999999"`
	// 下载价格
	Download int `json:"download" validate:"required,min=30,max=99999999"`
	// 市场价格
	Shop int `json:"shop" validate:"required,min=100,max=99999999"`
}
