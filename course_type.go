package core

const (
	// CourseTypeRecord 录播课
	CourseTypeRecord CourseType = 10
	// CourseTypePublic 公开课
	CourseTypePublic CourseType = 20
	// CourseTypeInteractive 互动课
	CourseTypeInteractive CourseType = 30
)

// CourseType 课程类型
type CourseType int
