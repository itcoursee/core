package bo

import (
	`gitea.com/itcoursee/core`
	`github.com/storezhang/gox`
)

// CoursePaging 课程分页对象
type CoursePaging struct {
	gox.Paging

	// 课程类型
	Type core.CourseType `json:"type" validate:"omitempty,oneof=10 20 30"`
	// 方向
	DirectionId int64 `json:"directionId,string" validate:"omitempty"`
	// 分类
	CategoryId int64 `json:"categoryId,string" validate:"omitempty"`
	// 阶段
	Stage core.CourseStage `json:"stage" validate:"omitempty,oneof=10 20 30 40"`

	// 排序字段
	Sort string `default:"id" json:"sort" validate:"omitempty,oneof=id"`
}

func (cp *CoursePaging) SortField() string {
	return cp.Sort
}
