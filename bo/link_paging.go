package bo

import (
	`github.com/storezhang/gox`
)

// LinkPaging 友链分页对象
type LinkPaging struct {
	gox.Paging

	// 排序字段
	Sort string `default:"id" json:"sort" validate:"omitempty,oneof=id"`
}

func (lp *LinkPaging) SortField() string {
	return lp.Sort
}
