package bo

import (
	`github.com/storezhang/gox`
)

// TagPaging 标签分页对象
type TagPaging struct {
	gox.Paging

	// 排序字段
	Sort string `default:"id" json:"sort" validate:"omitempty,oneof=id"`
}

func (tp *TagPaging) SortField() string {
	return tp.Sort
}
