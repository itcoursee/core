package core

// CourseContentTypeCover 课程封面
const CourseContentTypeCover CourseContentType = 10

// CourseContentType 课程内容类型
type CourseContentType int8
