package core

const (
	// CourseStageBeginner 零基础、初学者
	CourseStageBeginner CourseStage = 10
	// CourseStageElementary 初阶
	CourseStageElementary CourseStage = 20
	// CourseStageIntermediate 中阶
	CourseStageIntermediate CourseStage = 30
	// CourseStageAdvanced 高阶
	CourseStageAdvanced CourseStage = 40
)

// CourseStage 课程适用阶段
type CourseStage int8
