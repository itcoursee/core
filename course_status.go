package core

const (
	// CourseStatusCreated 已创建
	CourseStatusCreated CourseStatus = 10
	// CourseStatusUploading 上传中
	CourseStatusUploading CourseStatus = 20
	// CourseStatusUploaded 已上传
	CourseStatusUploaded CourseStatus = 21
	// CourseStatusOnSale 售卖中
	CourseStatusOnSale CourseStatus = 30
	// CourseStatusOffShelf 已下架
	CourseStatusOffShelf CourseStatus = 31
)

// CourseStatus 课程状态
type CourseStatus int
