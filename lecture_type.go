package core

const (
	// LectureTypeChapter 章节
	LectureTypeChapter LectureType = 10
	// LectureTypeLecture 讲次
	LectureTypeLecture LectureType = 20
)

// LectureType 章节、讲次类型
type LectureType int8
