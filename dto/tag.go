package dto

import (
	`gitea.com/itcoursee/core/bo`
)

type (
	// BaseTag 标签基础信息
	BaseTag struct {
		Seo

		// 名称
		Name string `mapstruct:"name" json:"name" validate:"required,max=64"`
	}
	// Tag 标签
	Tag struct {
		IdRsp
		BaseTag
	}

	// AddTagReq 添加单个标签请求
	AddTagReq struct {
		BaseTag
	}
	// AddTagRsp 添加单个标签响应
	AddTagRsp struct {
		IdRsp
	}

	// GetTagReq 获取单个标签请求
	GetTagReq struct {
		IdReq
	}
	// GetTagRsp 获取单个标签响应
	GetTagRsp struct {
		BaseTag
	}
	// GetsTagByPagingReq 分页获取标签列表请求
	GetsTagByPagingReq struct {
		bo.TagPaging
	}
	// GetsTagByPagingRsp 分页获取标签列表响应
	GetsTagByPagingRsp struct {
		// 总数
		Total int64 `json:"total"`
		// 数据
		Tags []*Tag `json:"tags"`
	}

	// UpdateTagReq 更新单个标签请求
	UpdateTagReq struct {
		IdReq
		BaseTag
	}
	// UpdateTagRsp 更新单个标签响应
	UpdateTagRsp struct {
		IdRsp
	}

	// DeleteTagReq 删除单个标签请求
	DeleteTagReq struct {
		IdReq
	}
	// DeleteTagRsp 删除单个标签响应
	DeleteTagRsp struct {
		IdRsp
	}
)
