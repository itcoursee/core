package dto

import (
	`gitea.com/itcoursee/core/bo`
)

type (
	// BaseLink 友链基础信息
	BaseLink struct {
		// 名称
		Name string `json:"name" validate:"required,max=64"`
		// 地址
		Url string `json:"url" validate:"required,url"`
	}
	// Link 标签
	Link struct {
		IdRsp
		BaseLink
	}

	// AddLinkReq 添加单个友链请求
	AddLinkReq struct {
		BaseLink
	}
	// AddLinkRsp 添加单个友链响应
	AddLinkRsp struct {
		IdRsp
	}

	// GetLinkReq 获取单个友链请求
	GetLinkReq struct {
		IdReq
	}
	// GetLinkRsp 获取单个友链响应
	GetLinkRsp struct {
		BaseLink
	}
	// GetsLinkByPagingReq 分页获取友链列表请求
	GetsLinkByPagingReq struct {
		bo.LinkPaging
	}
	// GetsLinkByPagingRsp 分页获取友链列表响应
	GetsLinkByPagingRsp struct {
		// 总数
		Total int64 `json:"total"`
		// 数据
		Links []*Link `json:"links"`
	}

	// UpdateLinkReq 更新单个友链请求
	UpdateLinkReq struct {
		IdReq
		BaseLink
	}
	// UpdateLinkRsp 更新单个友链响应
	UpdateLinkRsp struct {
		IdRsp
		BaseLink
	}

	// DeleteLinkReq 删除单个友链请求
	DeleteLinkReq struct {
		IdReq
	}
	// DeleteLinkRsp 删除单个友链响应
	DeleteLinkRsp struct {
		IdRsp
	}
)
