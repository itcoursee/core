package dto

// IdRsp 带编号的响应
type IdRsp struct {
	// 编号
	Id int64 `json:"id,string"`
}
