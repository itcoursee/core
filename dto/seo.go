package dto

// Seo 搜索引擎优化字段
type Seo struct {
	// 标签
	Label string `json:"label" validate:"required,alpha,max=16"`
	// 关键字
	Keywords []string `json:"keywords" validate:"required"`
	// 详细描述
	Description string `json:"description" validate:"required,max=1024"`
}
