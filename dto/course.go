package dto

import (
	`gitea.com/itcoursee/core`
	`gitea.com/itcoursee/core/bo`
)

type (
	// BaseCourse 课程基础数据
	BaseCourse struct {
		// 名称
		Name string `json:"name" validate:"required,max=128"`
		// 方向
		DirectionId int64 `json:"directionId,string" validate:"required"`
		// 分类
		CategoryId int64 `json:"categoryId,string" validate:"required"`
		// 阶段
		Stage core.CourseStage `json:"stage" validate:"required,oneof=10 20 30 40"`
		// 价格
		Price core.Price `json:"price" validate:"required"`
	}
	// Course 课程
	Course struct {
		IdRsp
		BaseCourse
	}

	// AddCourseReq 添加单个课程请求
	AddCourseReq struct {
		BaseCourse

		// 课程类型
		Type core.CourseType `json:"type" validate:"required,oneof=10 20 30"`
	}
	// AddCourseRsp 添加单个课程响应
	AddCourseRsp struct {
		IdRsp
	}

	// GetCourseReq 获取单个课程请求
	GetCourseReq struct {
		IdReq
	}
	// GetCourseRsp 获取单个课程响应
	GetCourseRsp struct {
		BaseCourse
	}
	// GetsCourseByPagingReq 分页获取课程列表请求
	GetsCourseByPagingReq struct {
		bo.CoursePaging
	}
	// GetsCourseByPagingRsp 分页获取课程列表响应
	GetsCourseByPagingRsp struct {
		// 总数
		Total int64 `json:"total"`
		// 数据
		Courses []*Course `json:"courses"`
	}

	// UpdateCourseReq 更新单个课程请求
	UpdateCourseReq struct {
		IdReq
		BaseCourse
	}
	// UpdateCourseRsp 更新课程的响应
	UpdateCourseRsp struct {
		IdRsp
	}

	// DeleteCourseReq 删除单个课程请求
	DeleteCourseReq struct {
		IdReq
	}
	// DeleteCourseRsp 删除单个课程响应
	DeleteCourseRsp struct {
		IdRsp
	}
)
