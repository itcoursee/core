package dto

// IdReq 带编号的请求
type IdReq struct {
	// 编号
	Id int64 `json:"id,string" param:"id" query:"id" form:"id" xml:"id" validate:"required"`
}
