package dto

type (
	// BaseCategory 分类基础数据
	BaseCategory struct {
		Seo

		// 名称
		Name string `json:"name" validate:"required,max=128"`
		// 父分类编号
		ParentId int64 `json:"parentId,string"`
		// 排序
		Sequence int `json:"sequence" validate:"required"`
	}
	// Category 分类
	Category struct {
		IdRsp
		BaseCategory
	}

	// AddCategoryReq 添加单个分类请求
	AddCategoryReq struct {
		BaseCategory
	}
	// AddCategoryRsp 添加单个分类响应
	AddCategoryRsp struct {
		IdRsp

		// 名称
		Name string `json:"name"`
	}

	// GetCategoryReq 获取单个分类请求
	GetCategoryReq struct {
		IdReq
	}
	// GetCategoryRsp 获取单个分类响应
	GetCategoryRsp struct {
		BaseCategory
	}
	// GetsCategoryChildrenReq 按父分类获取子分类列表请求
	GetsCategoryChildrenReq struct {
		// 编号，可空，当为0的时候表示获得第一层子分类
		Id int64 `json:"id,string" param:"id" query:"id" form:"id" xml:"id" validate:"omitempty"`
	}
	// GetsCategoryChildrenRsp 按父分类获取子分类列表请求
	GetsCategoryChildrenRsp struct {
		// 分类列表
		Categories []*Category `json:"categories"`
	}

	// UpdateCategoryReq 更新单个分类请求
	UpdateCategoryReq struct {
		IdReq
		BaseCategory
	}
	// UpdateCategoryRsp 更新单个分类响应
	UpdateCategoryRsp struct {
		IdRsp

		// 名称
		Name string `json:"name"`
	}

	// DeleteCategoryReq 删除单个分类请求
	DeleteCategoryReq struct {
		IdReq
	}
	// DeleteCategoryRsp 删除单个分类响应
	DeleteCategoryRsp struct {
		IdRsp
	}
)
